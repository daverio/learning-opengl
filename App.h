#ifndef APP_H_SKROBOP4
#define APP_H_SKROBOP4

#include "Context.h"
#include "Renderer.h"

class App
{
public:
	App(int width, int height, std::string title, bool debug);
	int run();

private:
	void update(float deltaTime);
	void updateLightPosition(float deltaTime);

	std::shared_ptr<Context> m_context;
	Renderer m_renderer;
};

#endif /* end of include guard: APP_H_SKROBOP4 */
