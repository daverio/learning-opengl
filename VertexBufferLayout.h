#ifndef VERTEXBUFFERLAYOUT_H_S4P5WYQK
#define VERTEXBUFFERLAYOUT_H_S4P5WYQK

#include <cstddef>
#include <glad/glad.h>

#include <vector>

struct LayoutElement
{
	GLint count;
	GLenum type;
	GLboolean normalized;
	std::size_t size;
};

class VertexBufferLayout
{
public:
	VertexBufferLayout() : m_stride{0} {}

	GLsizei getStride() const { return m_stride; }

	template<typename T>
	void addLayout(GLint count) = delete;
	void buildLayout();

private:
	std::vector<LayoutElement> m_elements;
	GLsizei m_stride;
};

template<>
void VertexBufferLayout::addLayout<GLfloat>(GLint count);


#endif /* end of include guard: VERTEXBUFFERLAYOUT_H_S4P5WYQK */
