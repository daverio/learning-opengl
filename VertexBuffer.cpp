#include "VertexBuffer.h"

void VertexBuffer::bind()
{
	glBindBuffer(GL_ARRAY_BUFFER, m_ID);
}

void VertexBuffer::unBind()
{
	glBindBuffer(GL_ARRAY_BUFFER, 0);
}
