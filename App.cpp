#include "App.h"
#include "Constants.h"

#include <GLFW/glfw3.h>

#include <algorithm>
#include <iostream>
#include <numeric>

static float yaw{0}, pitch{0}; //NOLINT
static bool resetFPS{false}, showFPS{false}; //NOLINT

App::App(int width, int height, std::string title, bool debug)
	  : m_context(std::make_shared<Context>(width, height, title, debug)),
		m_renderer(m_context)
{
	static int swapInterval{1};
	glfwSwapInterval(swapInterval);
	glfwSetKeyCallback(m_context->getWindow(),
		[](GLFWwindow*  /*window*/, int key, int /*unused*/, int action, int /*unused*/)
		{
			if(key == GLFW_KEY_T && action == GLFW_PRESS)
			{
				glfwSwapInterval(swapInterval);
				resetFPS = true;
				swapInterval = 1 - swapInterval;
			}
			if(key == GLFW_KEY_F3 && action == GLFW_PRESS)
			{
				showFPS = !showFPS;
				resetFPS = true;
			}
		});

	glfwSetCursorPosCallback(m_context->getWindow(),
		[](GLFWwindow*  /*window*/, double xpos, double ypos)
		{
			static float previousXPos{static_cast<float>(xpos)};
			static float previousYPos{static_cast<float>(ypos)};
			float deltaXPos{static_cast<float>(xpos) - previousXPos};
			float deltaYPos{static_cast<float>(ypos) - previousYPos};
			previousXPos = static_cast<float>(xpos);
			previousYPos = static_cast<float>(ypos);

			constexpr float sensitivity{0.001};
			float deltaYaw{-deltaXPos*sensitivity};
			float deltaPitch{deltaYPos*sensitivity};

			yaw += deltaYaw;
			if(yaw >= 2*pi)
			{
				yaw -= 2*pi;
			}
			if(yaw < 0)
			{
				yaw += 2*pi;
			}

			pitch += deltaPitch;
			if(pitch >= pi/2)
			{
				pitch = pi/2;
			}
			if(pitch <= -pi/2)
			{
				pitch = -pi/2;
			}
		});
}

void printFPS(float deltaTime, float percent = 1)
{

	constexpr float defaultDeltaTime{1/60.0F};
	constexpr size_t maxFrameNumber{0x4000};
	constexpr float updatePeriod{1.5};

	static std::vector<float> frameDurations(maxFrameNumber);
	static size_t index{0};
	static float averageDeltaTime{defaultDeltaTime};

	if(resetFPS)
	{
		index = 0;
		averageDeltaTime = defaultDeltaTime;
		resetFPS = false;
	}

	frameDurations[index] = deltaTime;
	index++;

	if(index*averageDeltaTime > updatePeriod || index == frameDurations.size())
	{
		auto last{frameDurations.begin() + index};

		constexpr float hundredPercent{100.0F};
		std::sort(frameDurations.begin(), last);
		auto max{frameDurations[index*(hundredPercent-percent)/hundredPercent]};
		auto min{frameDurations[index*percent/hundredPercent]};
		averageDeltaTime = std::accumulate(frameDurations.begin(), last,
			static_cast<decltype(*frameDurations.cbegin()) >(0)) / index;

		std::cout << "\nabs max: " << 1/frameDurations[0]
			<< "\n" << percent << "% max:  " << 1/min
			<< "\nabs min: " << 1/frameDurations[index-1]
			<< "\n" << percent << "% min:  " << 1/max
			<< "\navg:     " << 1/averageDeltaTime << std::endl;

		index = 0;
	}
}

void App::updateLightPosition(float deltaTime)
{
	constexpr float angularVelocity{0.5};
	constexpr glm::vec3 xAxis(1,0,0);
	constexpr glm::vec3 yAxis(0,1,0);
	auto dAngle{angularVelocity*deltaTime};

	static glm::vec4 position(0,3,0,1);

	m_renderer.setLightPosition(glm::vec3(position));

	position = glm::rotate(glm::mat4(1.0f), dAngle, xAxis)*position;
	position = glm::rotate(glm::mat4(1.0f), dAngle*2, yAxis)*position;
}

void App::update(float deltaTime)
{
	if(showFPS)
	{
		printFPS(deltaTime);
	}

	constexpr float speed{1.5};
	float deltaPos{speed * deltaTime};

	const auto& cameraRotation{m_renderer.getCameraRotation()};

	if(glfwGetKey(m_context->getWindow(), GLFW_KEY_W)==GLFW_PRESS)
	{
		m_renderer.incrementCameraPosition(-deltaPos*cameraRotation[2]);
	}
	if(glfwGetKey(m_context->getWindow(), GLFW_KEY_S)==GLFW_PRESS)
	{
		m_renderer.incrementCameraPosition(deltaPos*cameraRotation[2]);
	}
	if(glfwGetKey(m_context->getWindow(), GLFW_KEY_A)==GLFW_PRESS)
	{
		m_renderer.incrementCameraPosition(-deltaPos*cameraRotation[0]);
	}
	if(glfwGetKey(m_context->getWindow(), GLFW_KEY_D)==GLFW_PRESS)
	{
		m_renderer.incrementCameraPosition(deltaPos*cameraRotation[0]);
	}
	if(glfwGetKey(m_context->getWindow(), GLFW_KEY_SPACE)==GLFW_PRESS)
	{
		m_renderer.incrementCameraPosition(glm::vec3(0,deltaPos,0));
	}
	if(glfwGetKey(m_context->getWindow(), GLFW_KEY_LEFT_SHIFT)==GLFW_PRESS)
	{
		m_renderer.incrementCameraPosition(glm::vec3(0,-deltaPos,0));
	}

	updateLightPosition(deltaTime);
	m_renderer.setCameraDirection(pitch, yaw);
}

static float getDeltaTime()
{

	constexpr float defaultDeltaTime{1/60.0F};

	float currentTime{static_cast<float>(glfwGetTime())};
	static float previousTime{currentTime - defaultDeltaTime};
	float deltaTime{currentTime - previousTime};
	previousTime = currentTime;

	return deltaTime;
}

int App::run()
{

	while(!m_context->shouldClose())
	{
		update(getDeltaTime());

		m_renderer.draw();
		m_context->refresh();
	}

	return 0;
}
