#ifndef INDEXBUFFER_H_X9ACDUZY
#define INDEXBUFFER_H_X9ACDUZY

#include <glad/glad.h>

#include "Buffer.h"

class IndexBuffer : public Buffer
{
public:
	using Buffer::Buffer;
	virtual void bind() override;
	virtual void unBind() override;
};

#endif /* end of include guard: INDEXBUFFER_H_X9ACDUZY */
