#ifndef BUFFER_H_6AOGUCEN
#define BUFFER_H_6AOGUCEN

#include <glad/glad.h>

#include <vector>

class Buffer
{
	public:
		Buffer();
		virtual ~Buffer();

		template<typename T>
		Buffer(const std::vector<T>& data) : Buffer() { setData(data); }

		GLsizei getCount() { return m_count; }

		virtual void bind() = 0;
		virtual void unBind() = 0;

		template<typename T>
		void setData(const std::vector<T>& data)
		{
			m_count = data.size();

			auto elementSize{sizeof(T)};
			auto size{elementSize*m_count};

			glNamedBufferData(m_ID, size, data.data(), GL_STATIC_DRAW);
		}

	protected:
		GLuint m_ID;
		GLsizei m_count;
};

#endif /* end of include guard: BUFFER_H_6AOGUCEN */
