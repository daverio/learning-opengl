#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include <exception>
#include <stdexcept>
#include <iostream>

#include "Context.h"
#include "glDebugOutput.h"

unsigned int Context::m_count = 0;
Context* Context::currentInstance = nullptr;

Context::Context(int width, int height, const std::string& title, bool debug)
	: m_width(width), m_height(height)
{
	glfwSetErrorCallback([](int, const char* error)
			{ throw std::runtime_error(error); });

	if(m_count==0 && !glfwInit())
		throw std::runtime_error("glfwInit failed.");

	glfwWindowHint(GLFW_OPENGL_DEBUG_CONTEXT, debug);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 5);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	m_window = glfwCreateWindow(width, height, title.c_str(),
							nullptr, nullptr);

	if(!m_window)
	{
		glfwTerminate();
		throw std::runtime_error("Could not create GLFWwindow");
	}

	if(m_count==0)
	{
		glfwMakeContextCurrent(m_window);

		if(!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
			throw std::runtime_error("Could not initiate GLEW.");

		if(debug)
		{
			int flags; glGetIntegerv(GL_CONTEXT_FLAGS, &flags);
			if(flags & GL_CONTEXT_FLAG_DEBUG_BIT)
			{
				glEnable(GL_DEBUG_OUTPUT);
				glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS);
				glDebugMessageCallback(glDebugOutput,nullptr);
				glDebugMessageControl(GL_DONT_CARE, GL_DONT_CARE, GL_DONT_CARE,
						0, nullptr, GL_TRUE);
			}
			else
				throw std::runtime_error("Could not enable debug context.");
		}
	}

	glfwSwapInterval(0);

	currentInstance = this;
	glfwSetFramebufferSizeCallback(m_window, [](GLFWwindow*, int width, int height)
			{
				Context::getCurrentInstance()->updateViewPort(width, height);
			});

	glfwSetInputMode(m_window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);

	m_count++;
}

Context::~Context()
{
	if(m_count==1)
		glfwTerminate();
	m_count--;
}

bool Context::shouldClose() const
{
	return glfwWindowShouldClose(m_window);
}

void Context::refresh()
{
	glfwSwapBuffers(m_window);
	glfwPollEvents();
}

void Context::updateViewPort(int width, int height)
{
	m_width = width;
	m_height = height;
	glViewport(0, 0, width, height);
}

