#ifndef RENDERER_H_IV6MHB9G
#define RENDERER_H_IV6MHB9G

#include <glad/glad.h>
#include <glm/mat3x3.hpp>

#include <string>

#include "Context.h"
#include "IndexBuffer.h"
#include "Shader.h"
#include "Texture.h"
#include "VertexArray.h"
#include "VertexBuffer.h"

class Renderer
{
public:
	Renderer(std::shared_ptr<Context> context);

	void draw();

	static void setClearColor(float r, float g, float b);

	void setFOV(float angle) { m_FOV = angle; }
	void setCameraPosition(const glm::vec3& v) { m_cameraPosition = v; }
	void incrementCameraPosition(const glm::vec3& v) { m_cameraPosition += v; }
	void setCameraDirection(float pitch, float yaw);
	void setLightPosition(const glm::vec3&);

	const glm::mat3& getCameraRotation() { return m_cameraRotation; }

private:
	std::shared_ptr<Context> m_context;

	std::vector<Shader> m_shaders;
	std::vector<VertexArray> m_VAOs;
	std::vector<VertexBuffer> m_VBOs;
	std::vector<VertexBufferLayout> m_layouts;
	std::vector<IndexBuffer> m_IBOs;
	std::vector<Texture> m_textures;

	float m_FOV;
	glm::vec3 m_cameraPosition;
	glm::mat3 m_cameraRotation;
};

#endif /* end of include guard: RENDERER_H_IV6MHB9G */
