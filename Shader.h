#ifndef SHADER_H_Z8PDI3QF
#define SHADER_H_Z8PDI3QF

#include <glad/glad.h>
#include <glm/mat4x4.hpp>
#include <glm/gtc/type_ptr.hpp>

#include <unordered_map>
#include <string>

class Shader
{
public:
	Shader(std::string vertexShaderFilename,
			std::string fragmentShaderFilename);
	virtual ~Shader();

	void setUniform(const std::string& name, GLfloat value);
	void setUniform(const std::string& name, GLint value);
	void setUniform(const std::string& name, const glm::mat4& matrix);
	void setUniform(const std::string& name, const glm::mat3& matrix);
	void setUniform(const std::string& name, const glm::vec3& vector);

private:
	GLint getUniformLocation(const std::string& name);

	GLuint m_ID;
	std::unordered_map<std::string, GLint> m_uniformCache;
};

#endif /* end of include guard: SHADER_H_Z8PDI3QF */
