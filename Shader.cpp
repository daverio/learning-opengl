#include <fstream>
#include <iostream>
#include <stdexcept>

#include "Shader.h"
#include "glm/gtc/type_ptr.hpp"

namespace
{
	std::string readShader(const std::string& shaderFilename)
	{
		std::ifstream shaderFile(shaderFilename);
		std::string shaderCode;

		if(shaderFile.good())
			shaderCode = std::string(std::istreambuf_iterator<char>{shaderFile}, {});
		else
			throw std::runtime_error("Could not read \'" + shaderFilename + "\'." + "\n");

		return shaderCode;
	}

	GLuint compileShader(GLenum type, const std::string& shaderCode, const std::string& shaderFilename)
	{
		auto* pShaderCode{shaderCode.c_str()};
		auto shader{glCreateShader(type)}; 
		glShaderSource(shader, 1, &pShaderCode, NULL);

		glCompileShader(shader);

		GLint success;
		glGetShaderiv(shader, GL_COMPILE_STATUS, &success);
		if(success != GL_TRUE)
		{
			GLint length;
			glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &length);
			GLchar* log{(GLchar*)alloca(length*sizeof(GLchar))};
			glGetShaderInfoLog(shader, length, NULL, log);
			glDeleteShader(shader);

			throw std::runtime_error("In \'" + shaderFilename + "\': " + log + "\n");
		}
		return shader;
	}

	GLuint linkProgram(GLuint vertexShader, GLuint fragmentShader)
	{
		auto id{glCreateProgram()};
		glAttachShader(id, vertexShader);
		glAttachShader(id, fragmentShader);
		glLinkProgram(id);
		glDeleteShader(vertexShader);
		glDeleteShader(fragmentShader);

		GLint success;
		glGetProgramiv(id, GL_LINK_STATUS, &success);
		if(success != GL_TRUE)
		{
			GLint length;
			glGetProgramiv(id, GL_INFO_LOG_LENGTH, &length);
			GLchar* log{(GLchar*)alloca(length*sizeof(GLchar))};
			glGetProgramInfoLog(id, length, NULL, log);
			glDeleteProgram(id);

			throw std::runtime_error(log);
		}
		return id;
	}
}

Shader::Shader(std::string vertexShaderFilename,
		std::string fragmentShaderFilename)
{
	auto vertexShaderCode{readShader(vertexShaderFilename)};
	auto fragmentShaderCode{readShader(fragmentShaderFilename)};

	auto vertexShader{compileShader(GL_VERTEX_SHADER, vertexShaderCode, vertexShaderFilename)};
	auto fragmentShader{compileShader(GL_FRAGMENT_SHADER, fragmentShaderCode, fragmentShaderFilename)};
	
	m_ID = linkProgram(vertexShader, fragmentShader);
	glUseProgram(m_ID);
}

Shader::~Shader()
{
	glDeleteProgram(m_ID);
}

void Shader::setUniform(const std::string &name, GLfloat value)
{
	auto location{getUniformLocation(name)};
	glUniform1f(location, value);
}

void Shader::setUniform(const std::string &name, GLint value)
{
	auto location{getUniformLocation(name)};
	glUniform1i(location, value);
}

void Shader::setUniform(const std::string& name, const glm::mat4& matrix)
{
	auto location{getUniformLocation(name)};
	glUniformMatrix4fv(location, 1, GL_FALSE, glm::value_ptr(matrix));
}

void Shader::setUniform(const std::string& name, const glm::mat3& matrix)
{
	auto location{getUniformLocation(name)};
	glUniformMatrix3fv(location, 1, GL_FALSE, glm::value_ptr(matrix));
}

void Shader::setUniform(const std::string &name, const glm::vec3 &vector)
{
	auto location{getUniformLocation(name)};
	glUniform3fv(location, 1, glm::value_ptr(vector));
}

GLint Shader::getUniformLocation(const std::string& name)
{
	auto location{m_uniformCache.try_emplace(name, 0)};

	if(location.second)
	{
		auto result (location.first->second = glGetUniformLocation(m_ID, name.c_str()));
#ifdef DEBUG
		if(result == -1)
		{
			std::cout << "Uniform \"" << name << "\" not found\n";
		}
#endif 
		return result;
	}
	return location.first->second;
}
