#include <stb_image.h>

#include <iostream>
#include <stdexcept>

#include "Texture.h"

Texture::Texture(const std::string& imagePath, bool alpha)
{
	glCreateTextures(GL_TEXTURE_2D, 1, &m_ID);

	glTextureParameteri(m_ID, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTextureParameteri(m_ID, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTextureParameteri(m_ID, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTextureParameteri(m_ID, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	
	int width, height, nrChannels;
	stbi_set_flip_vertically_on_load(true);  
	GLubyte *data{stbi_load(imagePath.c_str(), &width, &height, &nrChannels, 0)};
	if(data)
	{
		GLint previousTexture;
		glGetIntegerv(GL_TEXTURE_BINDING_2D, &previousTexture);

		glBindTexture(GL_TEXTURE_2D, m_ID);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0,
				alpha ? GL_RGBA : GL_RGB, GL_UNSIGNED_BYTE, data);
		glGenerateMipmap(GL_TEXTURE_2D);

		glBindTexture(GL_TEXTURE_2D, previousTexture);
	}
	else
		throw std::runtime_error("Failed to load texture");

	stbi_image_free(data);
}

Texture::Texture(Texture&& texture)
{
	m_ID = texture.m_ID;
	texture.m_ID = 0;
}

Texture::~Texture()
{
	glDeleteTextures(1, &m_ID);
}

void Texture::bind(unsigned int i)
{
	glBindTextureUnit(i, m_ID);
}

void Texture::unBind()
{
	glBindTexture(GL_TEXTURE_2D, 0);
}
