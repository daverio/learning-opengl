#ifndef VERTEXARRAY_H_TJZRRIFI
#define VERTEXARRAY_H_TJZRRIFI

#include <glad/glad.h>

#include "VertexBufferLayout.h"


class VertexArray
{
public:
	VertexArray();
	virtual ~VertexArray ();

private:
	GLuint m_ID;
	//std::vector<VertexBufferLayout> layout;
};

#endif /* end of include guard: VERTEXARRAY_H_TJZRRIFI */
