#version 330 core

in vec2 v_textureCoordinate;
in vec3 v_normal;
in vec3 v_fragmentPosition;

out vec4 color;

uniform sampler2D u_texture0;
uniform sampler2D u_texture1;
uniform vec3 u_lightPosition;
uniform float u_ambientLight;
uniform vec3 u_viewPosition;

void main()
{
	vec3 normal = normalize(v_normal);
	vec3 lightDirection = normalize(u_lightPosition - v_fragmentPosition);

	float diffuseBrightness = max(dot(normal, lightDirection), 0.0);
	vec4 diffuseLight = diffuseBrightness*vec4(1, 1, 1, 1);

	vec3 viewDirection = normalize(u_viewPosition - v_fragmentPosition);
	vec3 reflectionDirection = reflect(-lightDirection, normal);
	float specularBrightness = 1*pow(max(dot(viewDirection, reflectionDirection), 0.0), 128);

	color = mix(texture(u_texture0, v_textureCoordinate), texture(u_texture1, v_textureCoordinate), 0.2);
	color *= (specularBrightness + diffuseBrightness + u_ambientLight);
	color *= 1;
	color = color/(color+1);
}
