#version 330 core

layout(location = 0) in vec3 position;
layout(location = 1) in vec2 textureCoordinate;
layout(location = 2) in vec3 normal;

uniform mat4 u_model;
uniform mat4 u_view;
uniform mat4 u_projection;
uniform mat3 u_normalModel;

out vec2 v_textureCoordinate;
out vec3 v_normal;
out vec3 v_fragmentPosition;

void main()
{
	v_textureCoordinate = textureCoordinate;
	v_normal = u_normalModel*normal;
	v_fragmentPosition = vec3(u_model*vec4(position, 1.0));
	gl_Position = u_projection*u_view*vec4(v_fragmentPosition, 1.0);
}
