#ifndef GLDEBUGOUTPUT_H_GCUB4DBM
#define GLDEBUGOUTPUT_H_GCUB4DBM

#include <glad/glad.h>
#include "GLFW/glfw3.h"

void APIENTRY glDebugOutput(GLenum source, GLenum type, unsigned int id,
                            GLenum severity, GLsizei length,
							const char* message, const void* userParam);

#endif /* end of include guard: GLDEBUGOUTPUT_H_GCUB4DBM */
