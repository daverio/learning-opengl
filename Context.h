#ifndef WINDOW_H_GBYURNDV
#define WINDOW_H_GBYURNDV

#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include <memory>
#include <string>

class Context
{
	public:
		Context(int width, int height, const std::string& title,
				bool debug = false);
		virtual ~Context(); 
		Context(Context&) = delete;

		GLFWwindow* getWindow() const { return m_window; }

		int getWidth() const { return m_width; }
		int getHeight() const { return m_height; }
		float getAspectRatio() const { return static_cast<float>(m_width)/m_height; }

		void updateViewPort(int width, int height);
		bool shouldClose() const;
		void refresh();

		static Context* getCurrentInstance() { return currentInstance; }

	private:
		static Context* currentInstance;
		GLFWwindow* m_window;
		static unsigned int m_count;
		int m_width;
		int m_height;
};

#endif /* end of include guard: WINDOW_H_GBYURNDV */
