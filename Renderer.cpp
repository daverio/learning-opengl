#include <glad/glad.h>
#include <glm/mat4x4.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include <fstream>
#include <iostream>

#include "Constants.h"
#include "Renderer.h"
#include "vendor/include/glm/ext/matrix_transform.hpp"

namespace
{
	std::vector<GLuint> makeCubeIndices()
	{
		constexpr std::size_t faceNbr{6};
		constexpr std::size_t verticesPerFace{6};
		constexpr std::size_t verticesPerSquare{4};

		std::vector<GLuint> cubeIndices(faceNbr*verticesPerFace);
		std::vector<GLuint> squareVerticesIndices{0,1,2,2,3,0};

		for(std::size_t i{0}; i<faceNbr; i++)
		{
			for(std::size_t j{0}; j<verticesPerFace; j++)
			{
				cubeIndices[j+i*verticesPerFace]=squareVerticesIndices[j]+verticesPerSquare*i;
			}
		}
		return cubeIndices;
	}

	std::vector<GLfloat> makeCubePositions(GLfloat cubeSize)
	{
		return std::vector<GLfloat>
		{
			 cubeSize, -cubeSize, -cubeSize, 0, 0,  0,  0, -1,//right bottom back
			-cubeSize, -cubeSize, -cubeSize, 1, 0,  0,  0, -1,//left  bottom back
			-cubeSize,  cubeSize, -cubeSize, 1, 1,  0,  0, -1,//left  top    back
			 cubeSize,  cubeSize, -cubeSize, 0, 1,  0,  0, -1,//right top    back

			-cubeSize, -cubeSize,  cubeSize, 0, 0,  0,  0,  1, //left  bottom front
			 cubeSize, -cubeSize,  cubeSize, 1, 0,  0,  0,  1, //right bottom front
			 cubeSize,  cubeSize,  cubeSize, 1, 1,  0,  0,  1, //right top    front
			-cubeSize,  cubeSize,  cubeSize, 0, 1,  0,  0,  1, //left  top    front

			-cubeSize, -cubeSize, -cubeSize, 0, 0, -1,  0,  0, //left  bottom back
			-cubeSize, -cubeSize,  cubeSize, 1, 0, -1,  0,  0, //left  bottom front
			-cubeSize,  cubeSize,  cubeSize, 1, 1, -1,  0,  0, //left  top    front
			-cubeSize,  cubeSize, -cubeSize, 0, 1, -1,  0,  0, //left  top    back

			 cubeSize, -cubeSize,  cubeSize, 0, 0,  1,  0,  0, //right bottom front
			 cubeSize, -cubeSize, -cubeSize, 1, 0,  1,  0,  0, //right bottom back
			 cubeSize,  cubeSize, -cubeSize, 1, 1,  1,  0,  0, //right top    back
			 cubeSize,  cubeSize,  cubeSize, 0, 1,  1,  0,  0, //right top    front

			-cubeSize,  cubeSize,  cubeSize, 0, 0,  0,  1,  0, //left  top    front
			 cubeSize,  cubeSize,  cubeSize, 1, 0,  0,  1,  0, //right top    front
			 cubeSize,  cubeSize, -cubeSize, 1, 1,  0,  1,  0, //right top    back
			-cubeSize,  cubeSize, -cubeSize, 0, 1,  0,  1,  0, //left  top    back

			-cubeSize, -cubeSize, -cubeSize, 0, 0,  0, -1,  0, //left  bottom back
			 cubeSize, -cubeSize, -cubeSize, 1, 0,  0, -1,  0, //right bottom back
			 cubeSize, -cubeSize,  cubeSize, 1, 1,  0, -1,  0, //right bottom front
			-cubeSize, -cubeSize,  cubeSize, 0, 1,  0, -1,  0 //left  bottom front
		};
	}
}
Renderer::Renderer(std::shared_ptr<Context> context)
	: m_context(std::move(context)), m_FOV(pi/4), m_cameraPosition(glm::vec3(0, 0, 4)),
	  m_cameraRotation(1)
{
	m_VAOs.emplace_back();

	constexpr GLfloat cubeSize{.5};
	m_VBOs.emplace_back(makeCubePositions(cubeSize));
	m_VBOs[0].bind();

	m_IBOs.emplace_back(makeCubeIndices());
	m_IBOs[0].bind();

	m_layouts.emplace_back();
	m_layouts[0].addLayout<GLfloat>(3);
	m_layouts[0].addLayout<GLfloat>(2);
	m_layouts[0].addLayout<GLfloat>(3);
	m_layouts[0].buildLayout();

	m_shaders.emplace_back("res/shader/shader.vert", "res/shader/shader.frag");
	m_shaders[0].setUniform("u_texture0", 0);
	m_shaders[0].setUniform("u_texture1", 1);

	m_textures.emplace_back("res/container.jpg", false);
	m_textures[0].bind(0);
	m_textures.emplace_back("res/arrow.jpg", false);
	m_textures[1].bind(1);

	constexpr GLfloat ambientLight(0.2);
	m_shaders[0].setUniform("u_ambientLight", ambientLight);
	constexpr glm::vec3 lightPosition(-0.5, 0.6, 0.6); //NOLINT
	m_shaders[0].setUniform("u_lightPosition", lightPosition);

	glEnable(GL_DEPTH_TEST);
}

void Renderer::draw()
{
	float constexpr near{0.1};
	float constexpr far{100};

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); //NOLINT: that's the conanical why of doing it with OpenGL

	auto viewTranslation{glm::translate(glm::mat4(1), -m_cameraPosition)};
	auto view{glm::transpose(glm::mat4(m_cameraRotation))*viewTranslation};
	auto projection{glm::perspective(m_FOV, m_context->getAspectRatio(), near, far)};
	m_shaders[0].setUniform("u_projection", projection);
	m_shaders[0].setUniform("u_view", view);
	m_shaders[0].setUniform("u_viewPosition", m_cameraPosition);
	
	constexpr auto model1{glm::mat4(1)};
	m_shaders[0].setUniform("u_model", model1);
	auto normalModel1{glm::mat3(glm::transpose(glm::inverse(model1)))};
	m_shaders[0].setUniform("u_normalModel", normalModel1);
	glDrawElements(GL_TRIANGLES, m_IBOs[0].getCount(), GL_UNSIGNED_INT, nullptr);

	auto model2{glm::rotate(glm::translate(model1, glm::vec3(1.5, 0, 0)), 2.5f, glm::vec3(1, 0, 1))}; //NOLINT
	m_shaders[0].setUniform("u_model", model2);
	auto normalModel2{glm::mat3(glm::transpose(glm::inverse(model2)))};
	m_shaders[0].setUniform("u_normalModel", normalModel2);
	glDrawElements(GL_TRIANGLES, m_IBOs[0].getCount(), GL_UNSIGNED_INT, nullptr);
}

void Renderer::setClearColor(float r, float g, float b)
{
	glClearColor(r, g, b, 1);
}

void Renderer::setCameraDirection(float pitch, float yaw)
{
	constexpr float safetyPitch{1.0F/32};
	if(pitch >= pi/2 - safetyPitch)
	{
		pitch = pi/2 - safetyPitch;
	}
	else if(pitch <= -(pi/2 - safetyPitch))
	{
		pitch = -(pi/2 - safetyPitch);
	}

	auto cameraZ{glm::vec3(std::sin(yaw)*std::cos(pitch),
	                                     std::sin(pitch),
	                       std::cos(yaw)*std::cos(pitch))};

	auto up{glm::vec3(0,1,0)};
	auto cameraX{glm::normalize(glm::cross(up, cameraZ))};
	auto cameraY{glm::cross(cameraZ, cameraX)};
	m_cameraRotation = glm::mat3(cameraX, cameraY, cameraZ);
}

void Renderer::setLightPosition(const glm::vec3& position)
{
	m_shaders[0].setUniform("u_lightPosition", position);
}
