#ifndef VERTEXBUFFER_H_BBZHETM7
#define VERTEXBUFFER_H_BBZHETM7

#include <glad/glad.h>

#include "Buffer.h"

class VertexBuffer : public Buffer
{
public:
	using Buffer::Buffer;
	virtual void bind() override;
	virtual void unBind() override;
};

#endif /* end of include guard: VERTEXBUFFER_H_BBZHETM7 */
